import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, Alert, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import foto from './src/assets/user.png';

export default function App() {

  function handleSocialNetwork(socialNetwork: string)
  {
    switch (socialNetwork)
    {
      case 'gitlab':
        Alert.alert('Meus Projetos', 'https://gitlab.com/gasparassi');
      break;
      case 'linkedin':
        Alert.alert('Meu LinkedIn', 'https://linkedin.com/in/elias-eder-gasparassi');
      break;
    }
  }

  return (
    <>
      <View style={styles.container}>
        <Image style={styles.photo} source={foto}/>
        <Text style={styles.name}>Elias Eder Gasparassi</Text>
        <Text style={styles.occupation}>Desenvolvedor Web</Text>
        <Text style={styles.stacks}>PHP | Laravel | MySQL | Docker | React</Text>
        <View style={styles.socialNetworks}>
          <TouchableOpacity onPress={() => handleSocialNetwork('gitlab')}>
            <Icon size={25} color='#8B008B' name='gitlab'/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleSocialNetwork('linkedin')}>
            <Icon size={25} color='#0000FF' name='linkedin'/>
          </TouchableOpacity>
        </View>
        <View style={styles.card}>
          <View style={styles.cardHeader}>
            <Text>Sobre mim</Text>
          </View>
          <View style={styles.cardContent}>
            <Text>
              Sou apaixonado por tecnologia e aficcionado por programação.
            </Text>
          </View>
        </View>
        <StatusBar style="auto" />
      </View>
    </>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    marginTop: '20%',
  },
  photo: {
    width: 200,
    height: 200,
  },
  name: {
    color: '#191970',
    fontSize: 26,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  occupation: {
    color: '#483D8B',
    marginBottom: 10,
  },
  stacks: {
    color: '#6959CD',
    marginBottom: 10,
  },
  socialNetworks: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '15%',
  },
  card: {
    width: '80%',
    height: '30%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#191970',
    marginTop: 20,
    padding: 10,
    backgroundColor: '#DCDCDC',
  },
  cardHeader: {
    paddingBottom: 10,
  },
  cardContent: {
    alignContent: 'center',
  },
});
